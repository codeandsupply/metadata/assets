# Marketing Style

## Repeated messaging

It is important to repeat messages because attention spans are low. But it's
important to not be repetitive.

Use different parts of the message in tweets about the same thing.

"doors for this thing open at 9pm"
"this thing is located at this place"
"at this thing, learn about this topic"
"our presenter for thing, person, does this thing"

All allowing the targeted market to research more on their own